#!/usr/bin/make -f
export DEB_BUILD_MAINT_OPTIONS = hardening=+all reproducible=+all
export DEB_CPPFLAGS_MAINT_APPEND = -include /usr/include/errno.h

%:
	dh $@ --with runit,sysuser

# On pickdns
# ~~~~~~~~~~
#
# Upstream release builds and install following programs:
#
#  * pickdns
#  * pickdns-conf
#  * pickdns-data
#
# which are not documented on upstream home page. According to[1], section 5.7,
# these programs are no longer needed as of djbdns-1.04, as its functionality
# is included into tinydns. So, Debian package do not install these binaries.
#
# [1] http://www.lifewithdjbdns.org/

override_dh_auto_configure:
	echo /usr | tee conf-home
	printf -- '%s\n' '$(CC) $(CPPFLAGS) $(CFLAGS)' | tee conf-cc
	printf -- '%s\n' '$(CC) $(LDFLAGS)' | tee conf-ld
ifeq ($(DEBEMAIL),<salsa-pipeline@debian.org>)
	echo 'int main () { return 0; }' | tee chkshsgr.c
endif

po-templates: po-templates-stamp
po-templates-stamp: deb-checkdir
	po2debconf debian/tinydns-run.templates.in \
	  >debian/tinydns-run.templates
	touch po-templates-stamp

execute_after_dh_auto_build:
	debian/aux/mk-init.d

# Upstream build system can't be configured to do staged installation:
# it has no distinction between prefix and $(DESTDIR). Since prefix is
# embedded into body of *-conf programs, this difference is significant.
#
# Actual installation is done by dh_install(1).
override_dh_auto_install:

# See [On pickdns]
execute_after_dh_install:
	rm -f debian/djbdns-conf/usr/sbin/pickdns-conf
	rm -f debian/djbnds-conf/usr/share/man/man8/pickdns-conf.8

# Upstream does strange thing -- its test suite assumes that programs
# are already installed. Normally, checks are performed between `build'
# and `install'.
#
# So automatic invocation of tests is inhibited, and they are invoked
# manuall at `install' stage.
#
# Instead, run our own test suite.
override_dh_auto_test:
	timelimit -p -t120 -T5 -- env PYTHONPATH='$(CURDIR)/debian/tests' python3 -B -u -m tinytest -b '$(CURDIR)' -s '$(CURDIR)' -S

override_dh_installinit:
	dh_installinit --no-enable --no-start

override_dh_installsystemd:
	dh_installsystemd --no-enable --no-start
